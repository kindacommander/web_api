CREATE TABLE tracks (
    id serial PRIMARY KEY NOT NULL,
    name VARCHAR(40) NOT NULL,
    author VARCHAR(30) NOT NULL,
    album VARCHAR(40) NOT NULL,
    genre VARCHAR(30) NOT NULL,
    lyrics TEXT NOT NULL,
    image_path TEXT NOT NULL,
    track_path VARCHAR(60) NOT NULL
);

CREATE TABLE users (
  id serial PRIMARY KEY NOT NULL,
  username VARCHAR(40) NOT NULL,
  password VARCHAR(20) NOT NULL  
);