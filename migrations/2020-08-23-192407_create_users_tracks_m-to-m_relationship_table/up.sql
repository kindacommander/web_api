-- Your SQL goes here
CREATE TABLE users_tracks (
    id serial PRIMARY KEY NOT NULL,
    user_id int REFERENCES users(id) NOT NULL,
    track_id int REFERENCES tracks(id) NOT NULL
);