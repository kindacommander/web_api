table! {
    tracks (id) {
        id -> Int4,
        name -> Varchar,
        author -> Varchar,
        album -> Varchar,
        genre -> Varchar,
        lyrics -> Text,
        image_path -> Text,
        track_path -> Varchar,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        password -> Varchar,
    }
}

table! {
    users_tracks (id) {
        id -> Int4,
        user_id -> Int4,
        track_id -> Int4,
    }
}

joinable!(users_tracks -> tracks (track_id));
joinable!(users_tracks -> users (user_id));

allow_tables_to_appear_in_same_query!(
    tracks,
    users,
    users_tracks,
);
