// use std::path::PathBuf;
use diesel;
use diesel::prelude::*;
use diesel::pg::PgConnection;

use crate::schema::tracks;

/*
#[derive(Serialize, Deserialize)]
pub enum Genre {
    Rap,
    Pop,
    Rock,
    Classic,
    // so on
}
*/

#[table_name = "tracks"]
#[derive(Serialize, Deserialize, Queryable, Insertable, AsChangeset)]
pub struct Track {
    pub name: String,
    pub author: String,
    pub album: String,
    pub genre: String,
    pub lyrics: String,
    pub image_path: String,
    pub track_path: String,
}

impl Track {
    pub fn add(track: Track, connection: &PgConnection) {
        diesel::insert_into(tracks::table)
            .values(&track)
            .execute(connection)
            .expect("Error adding new track");
    }

    pub fn delete(id: i32, connection: &PgConnection) -> bool {
        diesel::delete(tracks::table.find(id)).execute(connection).is_ok()
    }
}

#[table_name = "tracks"]
#[derive(Identifiable, Serialize, Deserialize, Queryable, AsChangeset, Clone)]
pub struct TrackWithId {
    pub id: i32,
    pub name: String,
    pub author: String,
    pub album: String,
    pub genre: String,
    pub lyrics: String,
    pub image_path: String,
    pub track_path: String,
}

impl TrackWithId {
    pub fn get_one_track(id: i32, connection: &PgConnection) -> TrackWithId {
        tracks::table.find(id).get_result::<TrackWithId>(connection).unwrap()
    }

    pub fn get_tracks_quantity(connection: &PgConnection) -> usize {
        tracks::table.order(tracks::id.asc()).load::<TrackWithId>(connection).unwrap().len()
    }

    pub fn get_track_list(limit: i32, connection: &PgConnection) -> Vec<TrackWithId> {
        tracks::table
            .order(tracks::id.asc())
            .limit(limit.into())
            .load::<TrackWithId>(connection)
            .expect("Error loading track list")
    }
}

#[derive(Serialize, Deserialize, Queryable, Clone)]
pub struct TrackRowInfo {
    pub id: i32,
    pub name: String,
    pub author: String, 
    pub image_path: String,
}

impl TrackRowInfo {
    pub fn get_track_list(limit: i32, connection: &PgConnection) -> Vec<TrackRowInfo> {
        tracks::table
            .select((tracks::id, tracks::name, tracks::author, tracks::image_path))
            .limit(limit.into())
            .load::<TrackRowInfo>(connection)
            .expect("Error loading track list")
    }
}