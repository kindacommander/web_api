use::diesel;
use diesel::associations;
use diesel::prelude::*;
use diesel::pg::PgConnection;
use diesel::query_dsl::BelongingToDsl;

use crate::schema::{users_tracks, users, tracks};

use crate::users::users::UserWithId;
use crate::tracks::track_info::TrackWithId;

#[derive(Serialize, Deserialize, Insertable)]
#[table_name = "users_tracks"]
pub struct UserTrack {
    pub user_id: i32,
    pub track_id: i32
}

impl UserTrack {
    pub fn add(u_id: i32, t_id: i32, connection: &PgConnection) -> String {
        let new_user_track = UserTrack {
            user_id: u_id, 
            track_id: t_id
        };
        
        diesel::insert_into(users_tracks::table)
            .values(&new_user_track)
            .execute(connection)
            .is_ok()
            .to_string()
    }
}

#[derive(Identifiable, Associations, Serialize, Deserialize, Queryable)]
#[belongs_to(UserWithId, foreign_key = "user_id")]
#[belongs_to(TrackWithId, foreign_key = "track_id")]
#[table_name = "users_tracks"]
pub struct UserTrackWithId {
    pub id: i32,
    pub user_id: i32,
    pub track_id: i32
}

impl UserTrackWithId {
    pub fn get_liked_tracks_list(u_id: i32, connection: &PgConnection) -> Vec<i32> {
        let user = users::table
            .find(u_id)
            .first::<UserWithId>(connection)
            .expect("Error loading user.");

        UserTrackWithId::belonging_to(&user)
            .inner_join(tracks::table)
            .select(tracks::id)
            .load::<i32>(connection)
            .expect("Error loading all belonging tracks id.")
    }

    pub fn find_liked_track_by_id(u_id: i32, t_id: i32, connection: &PgConnection) -> bool {
        let user = users::table
            .find(u_id)
            .first::<UserWithId>(connection)
            .expect("Error loading user.");

        match UserTrackWithId::belonging_to(&user)
            .inner_join(tracks::table)
            .select(tracks::id)
            .filter(tracks::id.eq(t_id))
            .first::<i32>(connection) {
                Ok(_) => true,
                Err(_) => false
            }
    }

    pub fn delete(u_id: i32, t_id: i32, connection: &PgConnection) -> String {
        diesel::delete(users_tracks::table.filter(users_tracks::user_id.eq(u_id)).filter(users_tracks::track_id.eq(t_id)))
            .execute(connection)
            .is_ok()
            .to_string()
    }
}