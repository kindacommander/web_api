use diesel;
use diesel::prelude::*;
use diesel::pg::PgConnection;

use crate::schema::users;

#[derive(Serialize, Deserialize, Queryable, Insertable, AsChangeset, Clone)]
#[table_name = "users"]
/// User model for db insertions, doesn't contain id field, which is serial in db.
pub struct User {
    pub username: String,
    pub password: String
}

impl User {
    pub fn add(user: User, connection: &PgConnection) -> Result<i32, String> {
        let new_username = user.username.clone();

        match diesel::insert_into(users::table).values(&user).execute(connection) {
            Ok(_) => Ok(users::table.select(users::id).filter(users::username.eq(new_username)).get_result::<i32>(connection).unwrap()),
            Err(_) => Err("Error creating new user".to_string())
        }
    }

    pub fn delete(id: i32, connection: &PgConnection) -> bool {
        diesel::delete(users::table.find(id)).execute(connection).is_ok()
    }
}


#[derive(Identifiable, Serialize, Deserialize, Queryable, AsChangeset, Clone)]
#[table_name = "users"]
/// User model for queries, contains id field.
pub struct UserWithId {
    pub id: i32,
    pub username: String,
    pub password: String
}

impl UserWithId {
    pub fn find(login: String, connection: &PgConnection) -> Result<i32, String> {
        match users::table.select(users::id).filter(users::username.eq(login.clone())).execute(connection).unwrap() {
            1 => Ok(users::table.select(users::id).filter(users::username.eq(login.clone())).get_result::<i32>(connection).unwrap()),
            0 => Err("There is no user with this username.".to_string()),
            _ => Err("Unknown error.".to_string()),
        }
    }

    pub fn verify(id: i32, passwd: String, connection: &PgConnection) -> Result<i32, String> {
        match users::table.filter(users::id.eq(id.clone())).filter(users::password.eq(passwd.clone())).execute(connection).unwrap() {
            1 => Ok(id),
            0 => Err("Wrong password.".to_string()),
            _ => Err("Unknown error.".to_string())
        }
    }
}