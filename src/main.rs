#![feature(proc_macro_hygiene, decl_macro)]

mod tracks;
mod users;
mod db;
mod schema;

#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate diesel;
#[macro_use] extern crate std;
#[macro_use] extern crate rocket_include_static_resources;
#[macro_use] extern crate rocket_contrib;

use rocket::response::Stream;
use std::fs::{File, rename};

use tracks::track_info::{Track, TrackWithId, TrackRowInfo};
use users::users::{User, UserWithId};
use users::users_tracks::{UserTrack, UserTrackWithId};

use serde::Serialize;

use rocket::{
    Data,
    http::ContentType,
};

use rocket_multipart_form_data::{
    mime,
    MultipartFormDataOptions,
    MultipartFormData,
    MultipartFormDataField,
};

use rocket_include_static_resources::StaticResponse;
use rocket_contrib::json::{Json, JsonValue};

use image_base64;

use unicode_segmentation::UnicodeSegmentation;

use rocket::http::{Cookie, Cookies};
use rocket::fairing::AdHoc;
use rocket::State;

// Tracks table manipulating methods

#[get("/")]
fn index() -> StaticResponse {
    static_response!("track-uploader")
}

struct DirConfig(String);

#[post("/add", data = "<data>")]
fn add_track(content_type: &ContentType, data: Data, tracks_repository_dir: State<DirConfig>, connection: db::Connection) {
    let options = MultipartFormDataOptions::with_multipart_form_data_fields(
        vec! [
            MultipartFormDataField::text("name"),
            MultipartFormDataField::text("author"),
            MultipartFormDataField::text("album"),
            MultipartFormDataField::text("genre"),
            MultipartFormDataField::text("lyrics"),
            MultipartFormDataField::file("track").size_limit(16 * 1000000).content_type_by_string(Some(mime::STAR_STAR)).unwrap(),
            MultipartFormDataField::file("image").size_limit(16 * 1000000).content_type_by_string(Some(mime::IMAGE_STAR)).unwrap(),
        ]
    );

    let mut multipart_form_data = MultipartFormData::parse(content_type, data, options).unwrap();

    // Data fields
    let name_field = multipart_form_data.texts.remove("name").unwrap().remove(0);
    let author_field = multipart_form_data.texts.remove("author").unwrap().remove(0);
    let album_field = multipart_form_data.texts.remove("album").unwrap().remove(0);
    let genre_field = multipart_form_data.texts.remove("genre").unwrap().remove(0);
    let lyrics_field = multipart_form_data.texts.remove("lyrics").unwrap().remove(0);
    let track_field = &multipart_form_data.files.remove("track").unwrap()[0];
    let image_field = &multipart_form_data.files.remove("image").unwrap()[0];
    
    // Fetching data
    let _name = name_field.text;
    let _author = author_field.text;
    let _album = album_field.text;
    let _genre = genre_field.text;
    let _lyrics = lyrics_field.text;
    let _track_name = track_field.file_name.clone().unwrap();
    let _image_name = image_field.file_name.clone().unwrap();

    // Saving image and audio files
    rename(&track_field.path, format!("{}audio/{}", &tracks_repository_dir.0, _track_name)).expect("Cannot save audio file");
    rename(&image_field.path, format!("{}images/{}", &tracks_repository_dir.0, _image_name)).expect("Cannot save image file");

    // Pushing fetched data into db
    let track_insert = Track {
        name: _name,
        author: _author,
        album: _album,
        genre: _genre,
        lyrics: _lyrics,
        track_path: _track_name,
        image_path: _image_name,
    };

    Track::add(track_insert, &connection);
}

#[get("/number/of/tracks")]
fn get_number_of_tracks(connection: db::Connection) -> Json<JsonValue> {
    Json(json!({"num": TrackWithId::get_tracks_quantity(&connection)}))
}

#[get("/track/<id>")]
fn stream(id: i32, tracks_repository_dir: State<DirConfig>, connection: db::Connection) -> Stream<File> {
    let found_track = TrackWithId::get_one_track(id, &connection);

    println!("Choosen track:{}", found_track.track_path);
    Stream::chunked(File::open(format!("{}audio/{}", &tracks_repository_dir.0, found_track.track_path)).unwrap(), 8192)
}

#[derive(Serialize)]
struct TrackInfo {
    id: i32,
    name: String,
    author: String,
    album: String,
    genre: String,
    lyrics: String,
    is_liked: bool,
    image: String,
}

#[get("/info/<t_id>/user/<u_id>")]
fn get_track_info(u_id: i32, t_id: i32, tracks_repository_dir: State<DirConfig>, connection: db::Connection) -> Json<TrackInfo> {
    let track = &TrackWithId::get_one_track(t_id, &connection);

    println!("path: {}", track.image_path.as_str());
    Json(TrackInfo {
        id: track.id,
        name: track.name.clone(),
        author: track.author.to_string(),
        album: track.album.to_string(),
        genre: track.genre.to_string(),
        lyrics: track.lyrics.to_string(),
        is_liked: UserTrackWithId::find_liked_track_by_id(u_id, t_id, &connection),
        image: UnicodeSegmentation::graphemes(image_base64::to_base64(format!("{}images/{}", &tracks_repository_dir.0, track.image_path).as_str()).as_str(), true)
                .skip(23).collect::<String>()
    })
}

#[derive(Deserialize)]
struct ListData {
    pub user_id: i32,
    pub limit: i32
}

#[derive(Serialize)]
struct TrackRow {
    pub id: i32,
    pub name: String,
    pub author: String,
    pub album: String,
    pub genre: String,
    pub lyrics: String,
    pub image_path: String,
    pub is_liked: bool,
}

#[post("/list", format = "json", data = "<data>")]
fn get_track_list(data: Json<ListData>, tracks_repository_dir: State<DirConfig>, connection: db::Connection) -> Json<Vec<TrackRow>> {
    let list_data = data.into_inner();
    let track_rows_info = TrackWithId::get_track_list(list_data.limit, &connection);
    let track_rows_info_iter = track_rows_info.iter();

    let mut track_list: Vec<TrackRow> = Vec::new();

    for row_info in track_rows_info_iter {

        let mut new_row_info = row_info.clone();
        new_row_info.image_path = UnicodeSegmentation::graphemes(image_base64::to_base64(format!("{}images/{}", &tracks_repository_dir.0, new_row_info.image_path).as_str()).as_str(), true)
        .skip(23).collect::<String>();

        track_list.push(TrackRow {
            id: new_row_info.id,
            name: new_row_info.name,
            author: new_row_info.author,
            album: new_row_info.album,
            genre: new_row_info.genre,
            lyrics: new_row_info.lyrics,
            image_path: new_row_info.image_path,
            is_liked: UserTrackWithId::find_liked_track_by_id(list_data.user_id, row_info.id, &connection)
        });
    }

    Json(track_list)
}

// Users table manipulating methods

#[post("/add", format = "json", data = "<user>")]
fn add_user(user: Json<User>, connection: db::Connection) -> String {
    let test_user = user.into_inner();

    User::add(test_user, &connection).unwrap().to_string()
}

#[get("/check_cookie")]
fn check_cookie(mut cookies: Cookies) -> String {
    match cookies.get_private("user_id") {
        Some(user_id) => user_id.to_string(),
        None => "Cookie is not found".to_string(),
    }
}


#[derive(Serialize, Deserialize)]
struct UserId {
    id: String
}

#[post("/get_cookie", format = "json", data = "<user_id>")]
fn get_cookie(mut cookies: Cookies, user_id: Json<UserId>) -> String {
    let fetched_id = user_id.into_inner();
    cookies.add_private(Cookie::new("user_id", fetched_id.id));
    std::mem::drop(cookies);

    "Returning cookie".to_string()
}

#[post("/login", format = "json", data = "<login_data>")]
fn login(login_data: Json<User>, connection: db::Connection) -> String {
    let fetched_login_data = login_data.into_inner();

    match UserWithId::find(fetched_login_data.username, &connection) {
        // if username is correct - check the password
        Ok(user_id) =>
            match UserWithId::verify(user_id, fetched_login_data.password, &connection) {
                Ok(user_id) => user_id.to_string(),
                Err(err_msg) => err_msg.to_string(),
            }
        // else - print the error message
        Err(error_msg) => return error_msg.to_string(),
    }
}

#[post("/logout")]
fn logout(mut cookies: Cookies) -> String {
    cookies.remove_private(Cookie::named("user_id"));
    std::mem::drop(cookies);
    String::from("You've successfully logged out.")
}

#[post("/like/<already_liked>", format = "json", data = "<user_track>")]
fn update_track_liked_status(user_track: Json<UserTrack>, already_liked: bool, connection: db::Connection) -> String {
    let new_user_track = user_track.into_inner();

    // if already_liked == true => delete user_track row(user disliked track)
    // if already_liked == false => insert new user_track row(user liked track)

    match already_liked {
        true => UserTrackWithId::delete(new_user_track.user_id, new_user_track.track_id, &connection),
        false => UserTrack::add(new_user_track.user_id, new_user_track.track_id, &connection)
    }
}

#[get("/<id>/liked_tracks")]
fn get_liked_tracks_list(id: i32, connection: db::Connection) -> String {
    format!("{:?}", UserTrackWithId::get_liked_tracks_list(id, &connection))
}

#[get("/<u_id>/check_liked_track/<t_id>")]
fn check_liked_track_by_id(u_id: i32, t_id: i32, connection: db::Connection) -> String {
    UserTrackWithId::find_liked_track_by_id(u_id, t_id, &connection).to_string()
}

fn main() {
    rocket::ignite()
        .attach(StaticResponse::fairing(|resources| {
            static_resources_initialize!(
                resources,
                "track-uploader",
                "/home/kindacommander/Development/Rust/Rocket/rocket_stream_test/front-end/static/index.html"
            );
        }))
        .attach(AdHoc::on_attach("Dir Config", |rocket| {
            let tracks_repository_dir = rocket.config()
                .get_str("tracks_repository_dir")
                .expect("Error: tracks repository dir not found.")
                .to_string();
            
            Ok(rocket.manage(DirConfig(tracks_repository_dir)))
        }))
        .mount("/", routes![index, add_track, stream, get_number_of_tracks])
        .mount("/track", routes![get_track_info, update_track_liked_status, get_track_list])
        .mount("/users", routes![add_user, get_cookie, check_cookie, login, logout, get_liked_tracks_list, check_liked_track_by_id])
        .manage(db::connect())
        .launch();
}
